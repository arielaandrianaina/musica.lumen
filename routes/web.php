<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/register', EmailController::class . '@sendMailInscription');
















































































/* -------------------------------------------------------------------------------------------- */
/* Itinéraires nommés */

// $router->get('profile', ['as' => 'profile', function () {
//     return "Hello";
// }]); 

// $router->get('profile', [
//     'as' => 'profile', 'uses' => 'UserController@showProfile'
// ]);

/* -------------------------------------------------------------------------------------------- */
/*Génération d'URL vers des routes nomées */

// $router->get('user/{id}/profile', ['as' => 'profile', function ($id) {
//     return $id;
// }]);

// $url = route('profile', ['id' => 1]);

/* -------------------------------------------------------------------------------------------- */
/*Groupes de routage */

// $router->group(['Middleware' => 'Authenticate'], function () use ($router) {
//     $router->get('/', function () {
//         return "Middleware Authenticate";
//     });

//     $router->get('user/profile', function () {
//         return "Uses Auth Middleware";
//     });
// });

/* -------------------------------------------------------------------------------------------- */
/*Espace de noms =namespace */

// $router->group(['namespace' => 'Admin'], function() use ($router)
// {
//     // Using The "App\Http\Controllers\Admin" Namespace...

//     $router->group(['namespace' => 'User'], function() use ($router) {
//         // Using The "App\Http\Controllers\Admin\User" Namespace...
//     });
// });

/* -------------------------------------------------------------------------------------------- */
/*Préfixes d'itinéraire */

// $router->group(['prefix' => 'admin'], function () use ($router) {
//     $router->get('users', function () {
//         // Matches The "/admin/users" URL
//         return "URL : http://localhost:8000/admin/users, URI : /admin/users";
//     });
// });

// $router->group(['prefix' => 'accounts/{accountId}'], function () use ($router) {
//     $router->get('detail', function ($accountId) {
//         // Matches The "/accounts/{accountId}/detail" URL
//         return "URL : http://localhost:8000/accounts/{ex:20}/detail, URI : /accounts/{ex:20}/detail";
//     });
// });







/* -------------------------------------------------------------------------------------------- */
//$router->get('/test-1', 'TestController@index');

// $router->get('/test-2', function () use ($router){
//     return "test-2";
// });

//with some differencies
// $router->get('/', function () use ($router) {
//     return $router->app->version();
// });
// $router->post('/', function () use ($router) {
//     return $router->app->version();
// });
// $router->put('/', function () use ($router) {
//     return $router->app->version();
// });
// $router->patch('/', function () use ($router) {
//     return $router->app->version();
// });
// $router->delete('/', function () use ($router) {
//     return $router->app->version();
// });

//with params
// $router->get('user/{id}', function ($id) use ($router) {
//     return "params ".$id;
// });

//using regular expression
// $router->get('posts/{postId}/comments/{commentId}', function ($postId, $commentId) {
//     return "parameter one is {$postId}, parameter two is {$commentId} comments";
// });

//optional path
// $router->get('/test[/{id}/detail]', function ($id) use ($router) {
//     return "optional path";
// });

// $router->get('user[/{name}]', function ($name = null) {
//     return $name;
// });

// $router->get('user/{name:[A-Za-z]+}', function ($name) {
//     return json_encode($name);
// });

//using middleware : ilay eo akaikin Controller io
// $router->get('/', ['Middleware' => 'Authenticate', function () {
//     return "optional path";
// }]);

//named route
// $router->get('/', ['as' => 'testpath', 'Middleware' => 'Authenticate', function () {
//     return "optional path";
// }]);

//route groups
// $router->group(['Middleware' => 'Authenticate'], function ($router) {
//     $router->get('/test1', function () use ($router){
//         return $router->app->version();
//     });
// });

//route prefixe : zany hoe http://localhost:8000/api/test2 (misy prefixe API vao mety)
// $router->group(['Middleware' => 'Authenticate', 'prefix' => 'api'], function ($router){
//     $router->get('/test1', function () use ($router) {
//         return "test 1";
//     });
//     $router->get('/test2', function () use ($router) {
//         return "test 2";
//     });
// });

//route namespace : io Admin io dia sous dossier anaty Controllers ary misy file TestController2
// $router->group(['namespace' => 'Admin'], function () use ($router){
//     $router->get('/super', 'TestController2@index');
// });
