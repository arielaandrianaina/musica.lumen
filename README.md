# MUSICA : 
Une application en architecture microservice pour une bibliotheque de musiques
***
## MUSICA LUMEN
- L'envoi d'un email après la création d'un compte
## Deploiement :
- Docker :
    - Dockerfile
- Kubernetes :
    - deploy.yml
    - service.yml

## URI 
Pour creer un compte
> registre?email={email} [[ GET ]]
