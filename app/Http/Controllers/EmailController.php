<?php
namespace App\Http\Controllers;

use App\Mail\MailInscription;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

class EmailController extends Controller
{
    public function __construct()
    {
        //
    }

    public function sendMailInscription(Request $request)
    {
        $response = new Response('ERROR', 400);
        try {
            $email = $request->query('email');
            Mail::to($email)->send(new MailInscription());
            $response = new Response('', 204);
        } catch (\Throwable $th) {
            $response = new Response('Une erreur est survenu dans le server', 505);
        }
        return $response;
    }
}