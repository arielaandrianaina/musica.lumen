# Utiliser l'image PHP 7.4 officielle de PHP
FROM php:7.4

# Copier les fichiers de l'application dans le conteneur
COPY . /var/www/html

# Définir le répertoire de travail
WORKDIR /var/www/html

# Installer les dépendances avec Composer
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    git \
    unzip \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Installer les dépendances de l'application
RUN composer install --no-interaction --no-plugins --no-scripts

# Exposer le port 80 pour l'application
EXPOSE 8088

# Lancer le serveur web PHP de développement
CMD ["php", "-S", "0.0.0.0:8088", "-t", "public"]
